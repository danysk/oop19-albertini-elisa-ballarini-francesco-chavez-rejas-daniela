package factory;

/**
 * Enum of types of controllers that can be created during the application.
 *
 */
public enum EnumFactory {
    /**
     * Enum that identifies the specific controller of the main menu of the application.
     */
    MENU,
    /**
     * Enum that identifies the specific controller of the setting part of the application.
     */
    SETTINGS,
    /**
     * Enum that identifies the specific controller of the custom piece builder part of the application.
     */
    CUSTOM,
    /**
     * Enum that identifies the speficif controller of the game of the application.
     */
    GAME,
    /**
     * Enum that identifies the specific controller of the login part of the application.
     */
    LOGIN,
    /**
     * Enum that identifies the specific controller for the profile part of a logged player in the application.
     */
    PROFILE

}
