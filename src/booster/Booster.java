package booster;

import playcontroller.PlayController;

/**
 * Interface for the booster object.
 */
public interface Booster {
    /**
     * Uses the Booster.
     * 
     * @param playController : The PlayController
     */
    void useBooster(PlayController playController);
}
