## TETRIS

L'applicazione si propone di implementare il classico gioco del Tetris con alcune funzioni aggiuntive, quali: aggiunta di pezzi "custom" creati dall'utente e aggiunta di booster attivabili tramite tastiera.
L'applicazione permette all'utente di registrarsi per accedere ai propri pezzi personalizzati e di salvare i propri risultati in classifica. Prima di avviare la partita l'utente ha la possibilità di scegliere il livello di difficoltà da cui partire e se utilizzare i pezzi personalizzati; ogni livello indica a quale velocità il pezzo inizierà a scendere. Durante la partita saranno a disposizione vari comandi da tastiera, quali: movimento e accelerazione dei pezzi, possibilità di attivare la holdbox e attivare dei booster. Il gioco dispone di 10 livelli, che modificano la velocità di discesa del pezzo, (arrivati all'ultimo livello si continua fino al game over) e si sale di livello aumentando il punteggio. Le righe eliminate (righe completate) fanno salire il punteggio.

---

## Autori

Daniela Chavez Rejas, 
Elisa Albertini, 
Francesco Ballarini, 
Michele Monti.

---

## Istruzione download

Assicurarsi che sia installata sul dispositivo una JRE funzionante. Scaricare il jar "Tetris.jar" nei Downloads.